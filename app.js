/* globals chrome */
(function (window, document, undefined) {
    // Hash of our removed videos, uses the original video's dom id.
    // Once removed, we set the value to false to indicate it should no longer be removed.
    var removedElements = {},
        SWEEP_INTERVAL = 500,
        timedSweep;

    /**
     * Placeholder click handler. Removes the placeholder element and restores the original content.
     */
    function onPlaceHolderClick(event) {
        event.target.parentElement.appendChild(removedElements[event.target.id]);
        event.target.removeEventListener('click', onPlaceHolderClick);
        event.target.remove();
        removedElements[event.target.id] = false;
    }

    /**
     * Helper to add the placeholder element.
     */
    function addPlaceHolder(id, parent) {
        var placeHolder;
        placeHolder = document.createElement('div');
        placeHolder.id = id; // persist the id of the original embeded video (this is the hash key)

        placeHolder.style.width = parent.style.width;
        placeHolder.style.height = parent.style.height;
        placeHolder.classList.add('removed-placeholder');
        placeHolder.innerHTML = chrome.i18n.getMessage('placeHolderText');
        placeHolder.addEventListener('click', onPlaceHolderClick);

        parent.parentElement.appendChild(placeHolder);
    }

    /*
     * Finds <embed> tags in the document and replace them with a placeholder div.
     */
    function sweepEmbeds() {
        window.clearInterval(timedSweep);

        var embeds,
            i,
            len,
            element,
            parent,
            id;

        embeds = document.getElementsByTagName('embed');
        len = embeds && embeds.length;

        for (i = 0; i < len; i++) {
            element = embeds[i];

            if (typeof element === 'undefined') {
                continue;
            }

            id = element.id;

            if (removedElements[id] === false) {
                continue;
            }

            parent = element.parentElement.parentElement.parentElement;// embeds great grandparent
            addPlaceHolder(id, parent);
            removedElements[id] = parent;
            parent.remove();
        }

        timedSweep = window.setInterval(sweepEmbeds, SWEEP_INTERVAL);
    }

    // Call initially to sweep out embeded videos on load.
    document.onreadystatechange = function() {
        if (document.readyState === 'complete') {
            sweepEmbeds();
        }
    };

    document.onscroll = sweepEmbeds;// continue to sweep them out as the user scrolls
})(window, document);


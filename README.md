facebook_video_placeholder
==========================

Replaced videos on your Facebook feed with a placeholder. You can then click on that placeholder to view the video.
